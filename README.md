# Matrix Ansible CD
A small Gitlab CI pipeline for automatic deployment of the [Matrix Docker Ansible Deploy](https://github.com/spantaleev/matrix-docker-ansible-deploy) playbook.

## How to use

1. Fork this repository
2. Create your config files as specified in the Matrix Docker Ansible Deploy's [documentation]((https://github.com/spantaleev/matrix-docker-ansible-deploy/blob/master/docs/README.md))
3. Add your SSH private key by creating a new file in the following location: `.ssh/id_rsa`
4. Run the pipeline, optionally configure scheduled regular deployments through the Gitlab UI

_A copy of my configuration can be found for reference in the folder `example`, obviously all secrets are replaced._

## Modifying the pipeline

The default pipeline found in `.gitlab-ci.yml`is configured such that all functionality is in the `.gitlab-ci-template.yml` file from the [Addono/Matrix Ansible CD](https://gitlab.com/Addono/matrix-ansible-cd) repository. Hence, modifying the local version of this Gitlab CI template file will not change the pipeline's functionality. Either:

* Replace the `include`reference in `gitlab-ci.yml`to your own version of this repository.
* Extend the functionality in `gitlab-ci.yml`by for example adding extra stages.
* Create a pull request upstream if you think your improvement would benefit everyone.

_Using the include allows me to have the pipeline public, while keeping my configuration repository private._

